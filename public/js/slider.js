$(document).ready(function(){
  $('.slider').slick({
  autoplay: true,
  autoplaySpeed: 3500,
  arrows: true,
  slidesToShow: 1,
  /*responsive: [
    {
      breakpoint: 992,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '50px',
        slidesToShow: 1
      }
    }
  ]*/
});

$('.sliderchico').slick({
  autoplay: true,
  autoplaySpeed: 3500,
  arrows: false,
  slidesToShow: 8,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        autoplay: true,
        arrows: false,
        slidesToShow: 4
      }
    },
    ]
});


$('.slick-prev').addClass("btn-slider prev");
$('.slick-prev').removeClass("slick-prev");
$('.btn-slider.prev').text("");
$('.btn-slider.prev').append('<i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i>');

$('.slick-next').addClass("btn-slider derecha");
$('.slick-next').removeClass("slick-next");
$('.btn-slider.derecha').text("");
$('.btn-slider.derecha').append('<i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>');



});


  
$( window ).on( "load", function() { 


$('.fa').removeClass('fa-2x');

  $('.xdebug-var-dump').remove();
  $( "#splash" ).fadeOut("slow");
  
});


  
  