

    $(document).ready(function(){
        
        $('.informacion').hide();
        $(".arriba img").hide();

        if($(window).width() >= $(window).height() && $(window).width()/2 <= $(window).height())
        {
            posicion=$('#galeria').position();

            ventana = $(window).height();

            ancho = ventana - posicion.top;

            $('#galeria').height(ancho);
            $('.items img').height(ancho);
        }else{
            $('.items img').height($('#galeria').height());
        }  
        ancla();

    });


    $('#mision').click(function()
    {
        if($('#mision_text').css('display') == 'none'){
            $('#vision_text').hide();
            $('#principios_text').hide();
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');

            $('#mision img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#mision_text').fadeIn('slow');
        }else{
            $('#mision_text').fadeOut('slow');
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
        
    });

    $('#vision').click(function()
    {
        if($('#vision_text').css('display') == 'none'){
            $('#mision_text').hide();
            $('#principios_text').hide();
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            
            $('#vision img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#vision_text').fadeIn('slow');
        }else
        {
            $('#vision_text').fadeOut('slow');
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
    });

    $('#principios').click(function()
    {
        if($('#principios_text').css('display') == 'none'){
            $('#mision_text').hide();
            $('#vision_text').hide();
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            
            $('#principios img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#principios_text').fadeIn('slow');
        }else
        {
            $('#principios_text').fadeOut();
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
    });


    function ancla()
    {
       $(".arriba img").delay(1500).fadeIn(); 
       $(".arriba img").delay(1000).fadeOut(); 
    }

    
    $(".arriba img").click(function()
        {
            $(".arriba img").hide(); 
    });

    $(".arriba").hover(function(){
      $(".arriba img").fadeIn();
    }, function(){
          $(".arriba img").fadeOut();
    });

    //Animacion desplazar a ancla
    $(function(){

        $('a[href*=#]').click(function() {

        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
             && location.hostname == this.hostname) {

                 var $target = $(this.hash);

                 $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

                 if ($target.length) {

                     var targetOffset = $target.offset().top;

                     $('html,body').animate({scrollTop: targetOffset}, 1000);

                     return false;

                }

           }

        });
    });
