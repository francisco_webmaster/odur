
  <link rel="stylesheet" type="text/css" href="{{asset("/plugins/slick/css/slick-theme.css")}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset("/plugins/slick/css/slick.css")}}"/>
  <script type="text/javascript" src="http://ewebapps.worldbank.org/apps/ip/Script%20Library/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="{{asset("/plugins/slick/js/jquery-migrate-1.2.1.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("/plugins/slick/js/slick.min.js")}}"></script>
  <!-- jquery Slider completo -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/slider.css') }}" />
  <script type="text/javascript" src="{{ asset('js/slider.js') }}">
  </script>
 
    <!-- Iconos importados de font-awesome !-->
  <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
