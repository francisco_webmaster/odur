<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{asset('libraries/slick/slick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('libraries/slick/slick-theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/trackpad-scroll-emulator.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/lib/circle-player/skin/circle.player.css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

    <title>Radio Unica Py</title>
</head>
<body>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=1967256716831604";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="main">
        <div class="container">
            <div class="contenedor-banner">
                <div class="row">
                    <div class="div-logo center-block col-sm-4 col-xs-12">
                        <img src="{{asset('img/img-logo.png')}}" class="center-block" alt="Radio Unica Py Logo" style="width:80%" />
                    </div>
                    <div class="div-redes col-sm-4 col-sm-offset-4 hidden-xs">
                        <div class="col-sm-4 col-sm-offset-3 col-sm-3">
                            <a href="https://www.instagram.com/radiounicapy/">
                                <img src="{{asset('img/img-instagram.png')}}" alt="Acceso al Instagram">
                            </a>
                        </div>
                        <div class="col-sm-4 col-sm-3">
                            <a href="https://www.facebook.com/Radio-%C3%9Anica-106808879938584/">
                                <img src="{{asset('img/img-facebook.png')}}" alt="Acceso a facebook">
                            </a>
                        </div>
                        <div class="col-sm-4 col-sm-3">
                            <a href="https://twitter.com/Radiounicapy">
                                <img src="{{asset('img/img-twitter.png')}}" alt="Acceso a twitter">
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="banner-superior">
                            @foreach($bannerssuperiores as $banner)
                                <img src="{{$banner->foto->url('superior')}}" alt="" />
                            @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="contenedor-radios text-center"> 
                    @foreach($radios as $radio)   
                        <div class="col-xs-4 div-radios-item">
                            <img src="{{asset('img/img-radio.png')}}" id="radio{{$radio->id}}" class="radio img-responsive" alt="Radio"/>
                            <p>{{$radio->nombre}}</p>
                            <div id="jquery_jplayer_{{$radio->id}}" class="cp-jplayer"></div>
                            <div id="cp_container_{{$radio->id}}" class="cp-container">
                                <div class="cp-buffer-holder">
                                    <div class="cp-buffer-1"></div>
                                    <div class="cp-buffer-2"></div>
                                </div>
                                <div class="cp-progress-holder">
                                    <div class="cp-progress-1"></div>
                                    <div class="cp-progress-2"></div>
                                </div>
                                <div class="cp-circle-control"></div>
                                <ul class="cp-controls">
                                    <li><a class="cp-play" tabindex="1">play</a></li>
                                    <li><a class="cp-pause" style="display:none;" tabindex="1">pause</a></li> 
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="row">
                <div class="banner-inferior">
                    @foreach($bannersinferiores as $banner)
                        <img src="{{$banner->foto->url('inferior')}}" alt="" />
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 text-center div-twitter">
                    <div class="fb-page" data-width="500" data-href="https://www.facebook.com/Radio-&#xda;nica-106808879938584/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Radio-&#xda;nica-106808879938584/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Radio-&#xda;nica-106808879938584/">Radio Única</a></blockquote></div>
                </div>
                <div class="col-sm-6 text-center div-twitter">
                    <a class="twitter-timeline" href="https://twitter.com/Radiounicapy">Tweets de Radiounicapy</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="cnt">
                    <div class="cnt-informacion text-center col-sm-2 col-sm-offset-2 col-xs-4" style="margin-top: 20px">
                        <a href="tel:+595213276520">
                            <span class="cnt-titulo">LLAMANOS</span>
                            <img src="{{asset('img/img-telefono.png')}}" alt="informacion para contactarse"/>
                            <span class="">021-3276520</span>
                        </a>
                    </div>
                    <div class="cnt-informacion text-center col-sm-7 col-xs-8" style="margin-top: 20px">
                        <span class="cnt-titulo col-sm-10">ESCRIBINOS</span>
                        <div class="row">
                            <div class="col-sm-5 col-xs-6">
                                <a href="tel:+595992805632">
                                    <img src="{{asset('img/img-whatsappn.png')}}" alt="informacion para contactarse"/>
                                    <span>0992-805632</span>
                                </a>
                            </div>                      
                            <div class="col-sm-5 col-xs-6">
                                <a href="mailto:hola@radiounicapy.live">
                                <img src="{{asset('img/img-correo.png')}}" alt="informacion para escribirnos"/>
                                <span>HOLA@RADIOUNICAPY.LIVE</span> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="row">
                <div class="div-footer-copyright col-sm-4 hidden-xs">
                    <p>Radio Unica / Todos los derechos reservados.</p>
                </div>
                <div class="div-footer-logo col-sm-4 text-center hidden-xs">
                    <div class="col-sm-10 col-sm-offset-1">
                        <img src="{{asset('img/img-logoverde.png')}}" alt="logo Radio Unica Py">
                    </div>
                </div>
                <div class="div-redes col-sm-3 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                    <div class="col-xs-4 col-sm-3">
                        <a href="https://www.instagram.com/radiounicapy/">
                            <img src="{{asset('img/img-instagram.png')}}" alt="Acceso al Instagram">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <a href="https://www.facebook.com/Radio-%C3%9Anica-106808879938584/">
                            <img src="{{asset('img/img-facebook.png')}}" alt="Acceso a facebook">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <a href="https://twitter.com/Radiounicapy">
                            <img src="{{asset('img/img-twitter.png')}}" alt="Acceso a twitter">
                        </a>
                    </div>
                </div>
                <div class="div-footer-logo col-xs-10 col-xs-offset-1 text-center hidden-sm hidden-lg" style="margin-top: 40px">
                    <div class="col-sm-10 col-sm-offset-1">
                        <img src="{{asset('img/img-logoverde.png')}}" alt="logo Radio Unica Py">
                    </div>
                </div>
            </div>
        </footer>
    </div>



    <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&key=AIzaSyAbTpB8wKFu0QZIC1t2_udPC0B5vcHtgeg" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('/js/jquery.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('/js/jquery.trackpad-scroll-emulator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/jquery.inlinesvg.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/jquery.affix.js')}}"></script>
    <script type="text/javascript" src="{{asset('/libraries/slick/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/wNumb.js')}}"></script>
    <script type="text/javascript" src="/dist/jplayer/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="/lib/circle-player/js/jquery.transform2d.js"></script>
    <script type="text/javascript" src="/lib/circle-player/js/jquery.grab.js"></script>
    <script type="text/javascript" src="/lib/circle-player/js/mod.csstransforms.min.js"></script>
    <script type="text/javascript" src="/lib/circle-player/js/circle.player.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.banner-superior').slick({
                infinite: true,
                speed: 500,
                fade: true,
                autoplay:true,
                cssEase: 'linear',
                adaptiveHeight: true
            });

            $('.banner-inferior').slick({
                dots: false,
                infinite: true,
                adaptiveHeight: true,
                speed: 300,
                autoplay:true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        dots: false,
                        infinite: true,
                        adaptiveHeight: true,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            });
        });


        $(document).ready(function() {

            $('.cp-container').hide();

            $('.radio').click(function(evt){
                $('.cp-container').hide();
                $('.radio').show();
                var numero = evt.target.id[5];
                if(numero != null){
                    $('#cp_container_'+numero).show();
                    $('#radio'+numero).hide();
                }
            });


            @foreach($radios as $radio)
                var myCirclePlayer{{$radio->id}} = new CirclePlayer("#jquery_jplayer_{{$radio->id}}",
                {
                    mp3:"{{$radio->url}}"
                }, {
                    cssSelectorAncestor: "#cp_container_{{$radio->id}}",
                    swfPath: "/dist/jplayer",
                    solution:'html, flash',
                    supplied: 'mp3',
                    wmode: "window",
                    keyEnabled: true
                });
            @endforeach

        });
    </script>
</body>
</html>