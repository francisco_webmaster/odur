<?php
    //Declaracion de variables
    $galeria[] = array();
    $enlaces[][] = array();
    $tituloenlace = '';
    $informaciones[][] = array();
    $menu[] = array();
    $redes[] = array();
    $i=0;
    $j=0;
    foreach($imagenes as $imagen)
    {
        if($imagen->categoria == 'galeria')
        {
            $galeria[$i] = $imagen->direccion;
            $i++;
        }else{
            if($imagen->categoria == 'enlaces')
            {
                $enlaces[$j][0] = $imagen->direccion;
                $enlaces[$j][1] = $imagen->hipervinculo;
                $j++;
            }  
        }
    }

    $i=0;
    $j=0;
    foreach($textos as $texto)
    {
        if($texto->categoria == 'titulos')
        {
            if($texto->titulo == 'Facebook')
            {
                $redes[0] = $texto->contenido;
            }else{
                if($texto->titulo == 'Instagram')
                {
                    $redes[1] = $texto->contenido;
                }else{
                    $menu[$i] = $texto->contenido;
                    $i++;
                }
            }
        }else{
            if($texto->categoria == 'enlaces')
            {
                $tituloenlace = $texto->titulo;
            }else{
                if($texto->categoria == 'informaciones')
                {
                    $informaciones[$j][0] = $texto->titulo;
                    $informaciones[$j][1] = $texto->contenido;
                    $j++;
                }
            }
        }
    }

    $ancho = "<script>document.write($(window).width())</script>"

?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <?php
            if($leng == 'es')
            {
        ?>
        <title>Servicio de Consultoría General</title>
        <?php
        }else{
            ?>
            <title>General Consulting Service</title>
        <?php
        }?>
        
        <!--LIBRERIAS -->
        <link href="http://allfont.es/allfont.css?fonts=arial-narrow" rel="stylesheet" type="text/css" />
        <!--CSS-->
        <link rel="stylesheet" href="{{ asset('css/principal.css')}}">
        <!-- Sliders -->
        @extends('partes.slider')

        <link rel="stylesheet" type="text/css" href="{{ asset('css/sliderchico.css') }}" />

        <!--Bootstrap GENERAL-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

        
        <!--Jquery-->
        <script src="{{ asset('plugins/jquery/jquery.js') }}"></script>

        <!-- Maps -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
        <link rel="icon" type="image/png" href="{{asset('img/logoempresa.ico')}}" />
    </head>
    <body>
        <script type="text/javascript">
        $(document).ready(function(){
            var cant = <?php echo $cantidad_enlaces; ?> ;
            if($(window).width()>992 && cant <=8)
            {
                //Validacion slider 
                $('#img_enlaces .slick-list').attr('style','width:100% !important');
                $('#img_enlaces .slick-track').attr('style','width:100% !important');
                $('.intereses').attr('style','width: 12.5% !important;height: auto !important;');
            }
            
        });
        </script>

        <!--Carga de pagina-->
        <div id="splash" >
            <table style="width: 100%;height: 100%;">
            <td>
            <img  src="{{asset('img/logoempresa.jpg')}}">
            </td>
            </table>
        </div>
        

        
        <!--Menu (PANEL 1)-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-center" id="cabecera">

            <div class="navbar-brand">
                <img src="{{asset('img/logoempresa.jpg')}}" style="width: 200px;height: auto;">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i style="color: #bc0000;" class="fa fa-align-justify fa-4x" aria-hidden="true"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" onclick="ancla()"
                        @if($leng == 'es')
                            href="{{url('/nosotros#nosotros')}}"
                        @else
                            href="{{url('/us#nosotros')}}"
                        @endif
                        >{{ $menu[0] }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="ancla()"
                        @if($leng == 'es')
                            href="{{url('/servicios#servicios')}}"
                        @else
                            href="{{url('/services#servicios')}}"
                        @endif
                        >{{ $menu[1] }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="ancla()"
                        @if($leng == 'es')
                            href="{{url('/clientes#clientes')}}"
                        @else
                            href="{{url('/clients#clientes')}}"
                        @endif
                        >{{ $menu[2] }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="ancla()" href="#enlaces">{{ $menu[3] }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="ancla()"
                            href="{{url('#primero')}}"
                        
                        >{{ $menu[4] }}</a>
                    </li>
                   
                </ul>
            </div>
            
        </nav>

        <!--Slider de Galerias (PANEL 2)-->
        @if(!empty($galeria[0]))
            <div class="container-fluid slider" id="galeria">
                @for($i=0;$i<2;$i++)
                    @for($j=0;$j<count($galeria);$j++)
                        <div class="items"><img src="{{$galeria[$j]}}" /></div>
                    @endfor
                @endfor
            </div> 
        @endif

        <!-- prueba -->
        <div id="primero">
            este es una prueba de div
        </div>
            


        <!-- Seccion de representadas (PANEL 3) -->
        <div class="container-fluid" id="enlaces">
        <a href="#cabecera"></a>
            <h1 class="titulos">
                {!! $tituloenlace !!}
            </h1>
            <div  class="row d-flex justify-content-center sliderchico" id="img_enlaces">
                @for($i=0;$i<count($enlaces);$i++)
                    <div class="intereses">
                        <a target="_blank" href="{{ $enlaces[$i][1] }}">
                            <img src="{{ $enlaces[$i][0] }}" />
                        </a>
                    </div>
                @endfor
            </div>         
        </div>
        

        <!--Informacion de la Empresa -->

        <div class="container-fluid informacion" id="mision_text"> 
            {!! $informaciones[0][1] !!}
        </div>

        <div class="container-fluid informacion" id="vision_text"> 
            {!! $informaciones[1][1] !!}
        </div>

        <div class="container-fluid informacion" id="principios_text"> 
            {!! $informaciones[2][1] !!}
        </div>


        
        <!-- Contacto (PANEL 7) -->
        <div class="container-fluid " id="contacto">
            <!-- Pie de página (PANEL 8) -->
            <div class="panel-footer" id="pie">
                <div id="redes">
                    <a target="_blank" href="{{ $redes[0] }}">
                        <img id="facebook" src="{{asset('img/logofacebook.png')}}" />
                    </a> 
                    <a target="_blank" href="{{ $redes[1] }}">
                        <img src="{{asset('img/logoinstagram.png')}}" />
                    </a>
                </div>
                <div>
                    @if($leng == 'es')
                        SCG / Todos los derechos reservados
                    @else
                        SCG / All rights reserved
                    @endif
                </div> 
                <div id="img_pie">
                    <img style="margin:0 auto;display: block;height: 80px;width: auto;" src="{{asset('img/logoempresablanco.png')}}" />
                </div>
            </div>
        </div>         
    </body>
</html>

<script type="text/javascript">


    $(document).ready(function(){
        
        $('.informacion').hide();
        $(".arriba img").hide();

        if($(window).width() >= $(window).height() && $(window).width()/2 <= $(window).height())
        {
            posicion=$('#galeria').position();

            ventana = $(window).height();

            ancho = ventana/2 - posicion.top;

            $('#galeria').height(ancho);
            //$('.items img').height(ancho);
            $('#galeria .slick-track').height(ancho);
        }else{
            //alert($('#galeria').height());
            $('.items img').height($('#galeria').height());
        }  
        ancla();

    });


    $('#mision').click(function()
    {
        if($('#mision_text').css('display') == 'none'){
            $('#vision_text').hide();
            $('#principios_text').hide();
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');

            $('#mision img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#mision_text').fadeIn('slow');
        }else{
            $('#mision_text').fadeOut('slow');
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
        
    });

    $('#vision').click(function()
    {
        if($('#vision_text').css('display') == 'none'){
            $('#mision_text').hide();
            $('#principios_text').hide();
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            
            $('#vision img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#vision_text').fadeIn('slow');
        }else
        {
            $('#vision_text').fadeOut('slow');
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
    });

    $('#principios').click(function()
    {
        if($('#principios_text').css('display') == 'none'){
            $('#mision_text').hide();
            $('#vision_text').hide();
            $('#mision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            $('#vision img').attr('src','img/Boton_Vision_y_otrosOFF.png');
            
            $('#principios img').attr('src','img/Boton_Vision_y_otros_ON.png');
            $('#principios_text').fadeIn('slow');
        }else
        {
            $('#principios_text').fadeOut();
            $('#principios img').attr('src','img/Boton_Vision_y_otrosOFF.png');
        }
    });


    function ancla()
    {
       $(".arriba img").delay(1500).fadeIn(); 
       $(".arriba img").delay(1000).fadeOut(); 
    }

    
    $(".arriba img").click(function()
        {
            $(".arriba img").hide(); 
    });

    $(".arriba").hover(function(){
      $(".arriba img").fadeIn();
    }, function(){
          $(".arriba img").fadeOut();
    });

    //Animacion desplazar a ancla
    $(function(){

        $('a[href*=#]').click(function() {

        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
             && location.hostname == this.hostname) {

                 var $target = $(this.hash);

                 $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

                 if ($target.length) {

                     var targetOffset = $target.offset().top;

                     $('html,body').animate({scrollTop: targetOffset}, 1000);

                     return false;

                }

           }

        });
    });


</script>
