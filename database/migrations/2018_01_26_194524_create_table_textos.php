<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTextos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('textos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('titulo');
            $table->text('contenido')->nullable();
            $table->enum('lenguaje',['es','in','']);
            $table->enum('categoria',['presentacion','servicios','clientes','contactos','enlaces','titulos','informaciones']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('textos');
    }
}
