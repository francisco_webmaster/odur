<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Texto;
use App\Imagen;
use Illuminate\Support\Facades\DB;

class PrincipalController extends Controller
{
    //
    public function index_es()
    {
    	$leng='es';
		$textos = DB::table('textos')->where([
            ['categoria', '=', 'enlaces'],
            ['lenguaje', '=', 'es'],
            ])->orWhere([
            ['categoria','=','informaciones'],
            ['lenguaje','=','es'], 
            ])->orWhere([
            ['categoria','=','titulos'],
            ['lenguaje','<>','in'], 
            ])->get();
		$imagenes = DB::table('imagenes')->where([
            ['categoria', '=', 'enlaces'],
            ])->orWhere([
            ['categoria','=','galeria'],
        ])->get();
        $enlaces = Imagen::where('categoria','enlaces')->get();
        $cantidad_enlaces = count($enlaces);
		return view('principal')->with('textos',$textos)->with('imagenes',$imagenes)->with('leng',$leng)->with('cantidad_enlaces',$cantidad_enlaces);        
    }
   
}
